# global configs
STAGE_SIZE = "1920x1080"
STAGE_WIDTH = 1920
STAGE_HEIGHT = 1080
CONTENT_WIDTH = 1600
CONTENT_HEIGHT = 800

NUM_RECORDS = 10   # number of records to display in the ranking window

RACE_TIME = 60 # duration of the race in seconds

RACE_DATA = "raceData.txt"

SCALE = STAGE_WIDTH/1920
