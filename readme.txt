#################################################
#					Cycling Race				#
#################################################

/* Objective: */

Cycling Race is a program that reads power data from an external interface and records it for a period of time (RACE_TIME).
The instant power, max power and energy are calculated and displayed as the race goes on.
At the end a summary displays the average power, max power and energy generated during the run.

The main screen display the ranking lists with records loaded from the file specified by RACE_DATA.
A user can start a race by pressing the button START RACE from the main window.

/* Instructions */
1. Change the settings in config.py.
2. Provide raceData.txt or an empty file.
3. To exit the program use CTRL+c.
4. F11 enters FullScreen. F10 exits full screen

/* Installation for  Raspberry Pi*/ 
1. Copy cyclingrace directory to the Documents folder
2. Copy start_cycling_race.sh to /etc/profile.d/ and make it executable
3. Reboot raspberry Pi

/* Current state: */
This program was developed to read data from an external interface.
You need to adapt GetSample function to your needs.
At the moment this function generates random values for the purpose of testing only.

/* Future work: */
1) Read data from an external interface, e.g. ANT+ device.


####################################################
#             Voltage Reader Class                 #
####################################################

Installation:
Start the I2C interface of the Raspberry Pi. If it is already open, skip this step. Open Terminal, type the following command, and press Enter:
	pi@raspberrypi:~ $ sudo raspi-config

Then use the up and down keys to select �5 Interfacing Options� -> �P5 I2C� and press Enter to confirm �YES�. Reboot the Raspberry Pi.
Installing Python libraries and git (networking required). If it is already installed, skip this step. In the Terminal, type the following commands, and press Enter:

	pi@raspberrypi:~ $ sudo apt update
	pi@raspberrypi:~ $ sudo apt upgrade
	pi@raspberrypi:~ $ sudo apt autoremove
	pi@raspberrypi:~ $ sudo apt-get install build-essential python-dev python-smbus git

Note: You might get a warning:
"Package python-smbus is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source". 
Try sudo apt install python3-smbus (you probably will have it installed already) and proceed.


Download the driver. In Terminal, type the following commands, and press Enter:
	pi@raspberrypi:~ $ git clone https://github.com/DFRobot/DFRobot_INA219.git

Run Sample Code
Connect the module to the Raspberry Pi according to the connection diagram. The I2C address defaults to 0x45, corresponding to "_INA219_I2C_ADDRESS4" in the code. If the I2C address needs to be modified, configure A0 and A1 to 0 or 1 respectively by the DIP switch on the module, and modify "_INA219_I2C_ADDRESS_x", the second parameter of "ina=INA219(1, _INA219_I2C_ADDRESS4)" in �getVoltageCurrentPower.py�. x can be 1, 2, 3 or 4. The mapping of DIP switch and the I2C address parameters are as follow:
_INA219_I2C_ADDRESS1: 0x40, A0=0, A1=0
_INA219_I2C_ADDRESS2: 0x41, A0=1, A1=0
_INA219_I2C_ADDRESS3: 0x44, A0=0, A1=1
_INA219_I2C_ADDRESS4: 0x45, A0=1, A1=1

Install DFRobot_INA219 Raspberry Pi library. In the Terminal, type the following commands and press Enter to run the sample code:
	pi@raspberrypi: $ cd ~/DFRobot_INA219/RaspberryPi/Python/example
	pi@raspberrypi:~/DFRobot_INA219/RaspberryPi/Python/example $ python getVoltageCurrentPower.py