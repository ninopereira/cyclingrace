import random
from tkinter import font
from helperFunctions import *
import sys
import time
from calibrated_sensor import CalibratedSensor
#from PowerSensorPackage import PowerSensor
#from smbus import SMBus # raspberry pi

#####################################
#          raspberry pi             #
#####################################
#voltage_sensor = PowerSensor()
#current_sensor_address = 0x48
#CHNL0      = 0x40  #Using Channel0 for current sensor over I2C (PCF8591)
#current_sensor = SMBus(1)
#current_sensor.write_byte(current_sensor_address, CHNL0)
#voltage_calibrated_sensor = CalibratedSensor('voltage_calibration_data.csv')
#current_calibrated_sensor = CalibratedSensor('current_calibration_data.csv')

#def GetVoltage():
#    global voltage_sensor
#    global voltage_calibrated_sensor
#    voltage = voltage_sensor.GetBusVoltage()
#    return voltage_calibrated_sensor.GetCalibratedData(voltage)
    
#def GetCurrent():
#    global current_sensor
#    global current_sensor_address
#    global current_calibrated_sensor
#    current = current_sensor.read_byte(current_sensor_address)
#    return current_calibrated_sensor.GetCalibratedData(current)

# raspberry pi
def GetSample():
    #voltage = GetVoltage()
    #current = GetCurrent()
    #power = voltage * current
    #return power
    return random.randint(0, 210)

bkgColor = Color.DARK
stageColor = Color.DARK_GREEN
titleColor = Color.DIRTY_YELLOW
textColor = Color.WHITE
dataColor = Color.LIGHT_GREEN

BUTTON_FG = Color.WHITE
BUTTON_BG = Color.GREEN

maxNameSize = 30
switchPeriod = 5

btHeight = 2
btWidth = 23

raceResult = RaceResult()

def ReplaceContent(previousWindow, newWindow):
    previousWindow.Hide()
    newWindow.Reset()

class Window():
    global stageColor
    global titleColor
    global textColor
    global dataColor

    def __init__(self, stage, bkgColor = "#000000"):
        self.bkgColor = bkgColor
        self.content = self.CreateContents(stage, bkgColor)
        self.buttonFrame = self.CreateButtonsFrame(self.content, bkgColor)
        self.Show()

    def Show(self):
        self.content.place(anchor='n', relx=0.5, rely=0.1)
        self.content.pack_propagate(0)
        self.content.grid_propagate(0)

    def Hide(self):
        self.content.place_forget()

    def CreateContents(self, stage, bkgColor):
        self.contents = Frame(stage, width=CONTENT_WIDTH, height=CONTENT_HEIGHT, bg=bkgColor, highlightbackground=textColor, highlightthickness=2)
        return self.contents

    def CreateButtonsFrame(self, stage, bkgColor):
        self.buttonFrame = Frame(stage, width=150, height=50, bg=bkgColor)
        self.buttonFrame.place(anchor='s', relx=0.5, rely=0.97)
        return self.buttonFrame

class RankingWindow(Window):

    def __init__(self, stage, bkgColor = "#000000"):
        super().__init__(stage, bkgColor)
        self.Initialise()

    def Reset(self):
        self.Show()
        if self.numTimers == 0 :
            self.LoadRecords()
            self = self.ReplaceRanks()

    def Initialise(self):
        self.visualiseCol = 1
        self.numTimers = 0

        # Title
        self.titleLabel = Label(self.content, text="Ranking - Top 10", font=header2, bg=self.bkgColor, fg=textColor)
        self.titleLabel.place(anchor='n', relx=0.5, rely=0.01)
    
        self.rankingFrame = Frame(self.content, bg=self.bkgColor)
        self.rankingFrame.place(anchor='n', relx=0.5, rely=0.15)

        self.LoadRecords()

        # Start button
        self.startButton = Button(self.buttonFrame, width=btWidth, height=btHeight, text="NEW RACE",
                                  fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3,
                                  command=lambda: ReplaceContent(self, inputWindow))
        self.startButton.grid(row=0, column=0, padx=10, pady=10)

        self.ReplaceRanks()

    def LoadRecords(self):
        # Load Records
        recordsData = LoadFromFile(RACE_DATA)
        powerData = GetPowerData(recordsData)
        energyData = GetEnergyData(recordsData)

        labels = ["# ", "Rider", " Energy (J) "]
        try:
            self.displayEnergyTable.destroy()
        except:
            pass
        self.displayEnergyTable = DisplayRecords(self.rankingFrame, energyData, labels, NUM_RECORDS, self.bkgColor, textColor, dataColor)
        self.displayEnergyTable.pack_forget()
        labels = ["# ", "Rider", "Max Power (W)"]
        try:
            self.displayPowerTable.destroy()
        except:
            pass
        self.displayPowerTable = DisplayRecords(self.rankingFrame, powerData, labels, NUM_RECORDS, self.bkgColor, textColor, dataColor)
        self.displayPowerTable.pack_forget()

    def ReplaceRanks(self):
        global switchPeriod
        if self.numTimers > 0:
            self.numTimers -= 1
        if self.content.winfo_manager() == "" : # if content is hidden
            self.displayPowerTable.pack_forget()
            self.displayPowerTable.pack_forget()
            return
        self.visualiseCol = (self.visualiseCol+1)%2
        if self.visualiseCol==0:
            self.displayEnergyTable.pack()
            self.displayPowerTable.pack_forget()
        else:
            self.displayEnergyTable.pack_forget()
            self.displayPowerTable.pack()

        if self.numTimers > 1:
            return
        else:
            self.content.after(switchPeriod*1000, lambda: self.ReplaceRanks())
            self.numTimers += 1
        return

class InputWindow(Window):
    global raceResult
    global rankingWindow
    global preRaceWindow
    global maxNameSize

    def __init__(self, stage, bkgColor = "#000000"):
        super().__init__(stage, bkgColor)
        self.Initialise()

    def Reset(self):
        self.Show()

    def Initialise(self):
        (label,inputName) = CreateInputBox(self.content, "Enter your name and press START RACE", self.bkgColor, textColor)   
    
        def GetName():
            if inputName.get():
                raceResult.userName = str(inputName.get()[:maxNameSize]) # limit name to 18 characters
                ReplaceContent(self, preRaceWindow)

        # buttons
        Button(self.buttonFrame, text="BACK TO RANKING", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3, command=lambda: ReplaceContent(self, rankingWindow)).grid(row=0, column=0, padx=10, pady=10)
        Button(self.buttonFrame, text="START RACE", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3, command=GetName).grid(row=0, column=1, padx=10, pady=10)

class PreRaceWindow(Window):
    global raceWindow
    def __init__(self, stage, bkgColor = "#000000"):
        super().__init__(stage, bkgColor)
        self.Initialise()

    def Reset(self):
        self.Show()
        self.countDown = 3
        self.countDownToStart.configure(text=self.countDown);        
        self.content.after((self.countDown+1)*1000, lambda: ReplaceContent(self, raceWindow))
        self.UpdateCountDownToStart()

    def Initialise(self):    
        # Display CountDown
        self.countDownToStart = Label(self.content, text="", bg=self.bkgColor, fg=textColor, font=huge)
        self.countDownToStart.pack(pady=120)

    def UpdateCountDownToStart(self):
        if self.countDown > 0:
            self.countDownToStart.configure(text=self.countDown);
            self.countDownToStart.after(1000, lambda: self.UpdateCountDownToStart())
        else:
            self.countDownToStart.configure(text="GO")
        self.countDown -= 1

def SaveRaceData(raceResult):
    record = [raceResult.userName, int(raceResult.totalEnergy), int(raceResult.maxPower)]
    SaveRecord(record, RACE_DATA)

def UpdateRacerData(countDownLabel, avgPowerLabel, maxPowerLabel, energyLabel, state='race'):
    global raceResult
    global stage
    countDownLabel.configure(text="{:.2f}".format(round(raceResult.countDown,2)))

    if state=='race':
        avgPowerLabel.configure(text="{:.2f}".format(round(raceResult.instantPower,2)))

    else:
        if (raceResult.iteration>0):
            avgPower = raceResult.totalPower/raceResult.iteration
        else:
            avgPower = 0
        avgPowerLabel.configure(text="{:.2f}".format(round(avgPower,2)))
    
    maxPowerLabel.configure(text="{:.2f}".format(round(raceResult.maxPower,2)))
    energyLabel.configure(text="{:.2f}".format(round(raceResult.totalEnergy,2)))

class RaceWindow(Window):
    global resultsWindow
    global textColor
    global raceResult
    
    def __init__(self, stage, bkgColor = "#000000"):
        super().__init__(stage, bkgColor)
        self.Initialise()

    def Reset(self):
        self.state='race'
        self.Show()
        interval = 0.05 # in seconds
        raceResult.SetRaceTime(RACE_TIME)
        raceResult.ResetValues()
        self.raceInfo.userNameLabel.configure(text=raceResult.userName)
        self.previousTime = time.time()

        self.UpdateCounter(interval)
        self.CheckEndOfRace()
    
    def Initialise(self):
        self.dataSampler = DataSampler(10, GetSample)
        self.state='race'
        # Create Race Info
        topMessage = "Do your best. Time is running out..."
        self.raceInfo = RaceInfo(self.content, topMessage, raceResult, self.state, self.bkgColor, textColor, dataColor)

        # Buttons
        Button(self.buttonFrame, text="ABORT", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3,command=lambda:  self.Abandon(rankingWindow)).grid(row=0, column=0, padx=10, pady=10)
        Button(self.buttonFrame, text="STOP EARLY", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3,command=lambda: self.StopEarly(raceResult, resultsWindow)).grid(row=0, column=1, padx=10, pady=10)
        Button(self.buttonFrame, text="RESTART", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3, command=lambda:  self.Abandon(preRaceWindow)).grid(row=0, column=2, padx=10, pady=10)
    
    def UpdateCounter(self, interval):
        global raceResult
        elapsed_time = time.time() - self.previousTime
        self.previousTime = time.time()
        if self.state == 'race':
            sample = self.dataSampler.GetDataSample()
            raceResult = ExtendSample(sample, raceResult, elapsed_time)
            raceResult.countDown -= elapsed_time
            if raceResult.countDown < 0 :
                SaveRaceData(raceResult)
                self.state = 'final'
                raceResult.countDown = 0
            else:
                self.raceInfo.UpdateGauges(raceResult, self.bkgColor)
                self.raceInfo.countDownLabel.after(int(interval*1000), lambda: self.UpdateCounter(interval))
        UpdateRacerData(self.raceInfo.countDownLabel, self.raceInfo.avgPowerLabel, self.raceInfo.maxPowerLabel, self.raceInfo.energyLabel, self.state)
        

    def CheckEndOfRace(self):
        if self.content.winfo_manager() == "" : # if content is hidden
            return
        if self.state=='final':
            ReplaceContent(self, resultsWindow)
        else:
            self.content.after(1000, lambda: self.CheckEndOfRace())

    def StopEarly(self, raceResult, nextWindow):
        self.state = 'final'
        SaveRaceData(raceResult)
        ReplaceContent(self, nextWindow)
    
    def Abandon(self, nextWindow):
        self.state = 'final'
        ReplaceContent(self, nextWindow)

class ResultsWindow(Window):
    global rankingWindow
    global inputWindow
    global preRaceWindow
    global raceWindow
    global raceResult
    global textColor

    def __init__(self, stage, bkgColor = "#000000"):
        super().__init__(stage, bkgColor)
        self.Initialise()

    def Reset(self):
        self.raceInfo.userNameLabel.configure(text=raceResult.userName)
        self.raceInfo.countDownLabel.pack_forget()
        
        UpdateRacerData(self.raceInfo.countDownLabel, self.raceInfo.avgPowerLabel, self.raceInfo.maxPowerLabel, self.raceInfo.energyLabel, 'race')
        self.raceInfo.UpdateGauges(raceResult, self.bkgColor)
        self.Show()

    def Initialise(self):
        topMessage = "Well done. Here's your results."

        self.raceInfo = RaceInfo(self.content, topMessage, raceResult, 'final', self.bkgColor, textColor, dataColor)

        UpdateRacerData(self.raceInfo.countDownLabel, self.raceInfo.avgPowerLabel, self.raceInfo.maxPowerLabel, self.raceInfo.energyLabel, 'final')
        
        # Add Ranking Position
        # todo

        # Buttons
        Button(self.buttonFrame, text="CONTINUE", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3,command=lambda:  ReplaceContent(self, rankingWindow)).grid(row=0, column=0, padx=10, pady=10)
        Button(self.buttonFrame, text="RACE AGAIN", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3, command=lambda:  ReplaceContent(self, preRaceWindow)).grid(row=0, column=1, padx=10, pady=10)
        Button(self.buttonFrame, text="NEW RACE", width=btWidth, height=btHeight, fg=BUTTON_FG, bg=BUTTON_BG, font=btFont, borderwidth=3, command=lambda:  ReplaceContent(self, inputWindow)).grid(row=0, column=2, padx=10, pady=10)

def SaveRecord(record, filename):
    f=open(filename,"a")
    f.write(record[0]+","+str(record[1])+","+str(record[2])+"\n")
    f.close()

def SecondsToHours(seconds):
    return (seconds / 3600)

def WatthToJoules(energyWh):
    return (energyWh * 3600)

def ExtendSample(sample, raceResult, elapsedTime): # elapsedTime in seconds
    # todo: get data from sensors
    raceResult.instantPower = sample
    raceResult.maxPower = max(raceResult.instantPower, raceResult.maxPower)
    raceResult.totalEnergy += elapsedTime*raceResult.instantPower
    raceResult.totalPower += raceResult.instantPower
    raceResult.iteration += 1
    return raceResult

def MainStage():
    global bkgColor
    global textColor
    global titleColor
    global stageColor
    # Set the stage
    stage = ConfigureStage("Energy Race", STAGE_SIZE, bkgColor)
    date  = InsertDate(stage, bkgColor, textColor)
    title = InsertTitle(stage, "Energy Race", bkgColor, titleColor)
    (logo, pic) = InsertLogo(stage)
    return (stage, logo)

def main():
    global rankingWindow
    global inputWindow
    global preRaceWindow
    global raceWindow
    global resultsWindow
    global powerSensor
    global stage

    (stage, logo) = MainStage()
    
    rankingWindow = RankingWindow(stage, stageColor)
    rankingWindow.Hide()

    inputWindow = InputWindow(stage, stageColor)
    inputWindow.Hide()

    preRaceWindow = PreRaceWindow(stage, stageColor)
    preRaceWindow.Hide()

    raceWindow = RaceWindow(stage, stageColor)
    raceWindow.Hide()
    
    resultsWindow = ResultsWindow(stage, stageColor)
    resultsWindow.Hide()

    rankingWindow.Show()
    stage.mainloop()

    
if __name__ == "__main__":
    main()
