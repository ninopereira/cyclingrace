# Class to get readings from I2C device https://wiki.dfrobot.com/Gravity%3A%20I2C%20Digital%20Wattmeter%20SKU%3A%20SEN0291
#
# ShuntVoltage: Voltage of the sampling resistor, IN+ to NI-.
# BusVoltage: Voltage of IN- to GND.
# Current: Current flows across IN+ and IN-. If the current flows from IN+ to IN-, the reading is positive. If the current flows from IN- to IN+, the reading is negative.
# Power: The product of "BusVoltage" and "Current", that is, power. The power resolution read directly from the module is 20mW (hardware mode). 
# If the power is obtained by using the statement "Power = BusVoltage*Current;", the resolution can be increased to 4mW (software mode).
#
# Install dependencies: 
# $sudo apt-get install build-essential python-dev python-smbus git
# $get DFRobot_INA219.py from github:
# git clone https://github.com/DFRobot/DFRobot_INA219.git

import sys
import time
from DFRobot_INA219 import INA219

# Modify these to calibrate according to actual reading of the INA219 and the multimeter
# for linear calibration
ina219_reading_mA = 1000
ext_meter_reading_mA = 1000

#ina.reset()                                # Resets all registers to default values

# initialise comms
class PowerSensor:
    def __init__(self):
        self.ina = INA219(1, INA219.INA219_I2C_ADDRESS4) # Change I2C address by dialing DIP switch
        self.initialised = False

    def Initialise(self):
        for i in range(5): # try connecting to I2C device for up to 5s.
            if self.ina.begin():
                #apply calibration values
                self.ina.linear_cal(ina219_reading_mA, ext_meter_reading_mA)
                self.initialised = True
                return True
            else:
                time.sleep(1)
        return False
    def SendWarning(self):
        print ("Device not initialised. Please try restarting the device and call Initialise function")
    def GetShuntVoltage(self):
        if self.initialised == True:
            return self.ina.get_shunt_voltage_mV() # in mV
        else:
            SendWarning()
    def BusVoltage(self):
        if self.initialised == True:
            return self.ina.get_bus_voltage_V() # in V
        else:
            SendWarning()
    def GetCurrent(self):
        if self.initialised == True:
            return self.ina.get_current_mA() # in mA
        else:
            SendWarning()
    def GetPower(self):
        if self.initialised == True:
            return self.ina.get_power_mW() # in mW
        else:
            self.SendWarning()
