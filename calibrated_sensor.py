import pwlf
from numpy import genfromtxt

class CalibratedSensor:

    def __init__(self, calibrationFile):
        self.calibrationData = genfromtxt(calibrationFile, delimiter=',', comments="#")
        x = self.calibrationData[0]
        y = self.calibrationData[1]
        self.calibrated_sensor = pwlf.PiecewiseLinFit(x, y)
        breaks = self.calibrated_sensor.fit(4*len(x))

    def GetCalibratedData(self, data):
        return self.calibrated_sensor.predict(data)

def TestCalibratedSensor():
    current_calibrated_sensor = CalibratedSensor('current_calibration_data.csv')
    print(current_calibrated_sensor.GetCalibratedData(3500))
    voltage_calibrated_sensor = CalibratedSensor('voltage_calibration_data.csv')
    print(voltage_calibrated_sensor.GetCalibratedData(22))

def main():
    TestCalibratedSensor()

if __name__ == "__main__":
    main()