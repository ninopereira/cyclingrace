# Set of helper functions
from tkinter import *
from tkinter.ttk import *
from tkinter import PhotoImage, Frame, Button, Entry, Label
import PIL
from PIL import Image, ImageTk

import datetime as dt
from time import strftime
import copy
from enum import Enum
from cycle_config import *
import os
import sys
#from PowerSensorPackage import PowerSensor

# text formats
huge    = ('Helvatical bold', int(300*SCALE))  # 300
header1 = ('Helvatical bold',  int(40*SCALE))  # 40
header2 = ('Helvatical bold',  int(32*SCALE))  # 32
header3 = ('Helvatical bold',  int(26*SCALE))  # 26
body    = ('Helvatical bold',  int(24*SCALE))  # 24
btFont  = ('Helvatical bold',  int(20*SCALE))  # 20


#global powerSensor

# Create power sensor device
#powerSensor = PowerSensor()
#if not powerSensor.Initialise():
#    exit()

class Color(str, Enum):
    GREEN = "#2f7a6e"
    DARK_GREEN = "#172b28"
    LIGHT_GREEN = "#16c9af"
    VERY_LIGHT_GREEN = "#c7fff7"
    DIRTY_YELLOW = "#babf1f"
    BLUE = "#284266"
    RED = "#dc3545"
    DARK_BLUE = "#1A2238"
    DARK = "#151515"
    LIGHT_BLUE = "#9DAAF2"
    ORANGE = "#FF6A3D"
    YELLOW = "#F4DB7D"
    WHITE = "#FFFFFF"
    GREY = "#DDDDDD"

class RaceResult:
    userName = ""
    instantPower = 0
    maxPower = 0
    totalPower = 0
    totalEnergy = 0
    countDown = 0
    iteration = 0

    def ResetValues(self):
        self.instantPower = 0
        self.maxPower = 0
        self.totalEnergy = 0
        self.totalPower = 0
        self.iteration = 0

    def SetRaceTime(self, raceDurationMs):
        self.countDown = raceDurationMs

# global vars
raceResult = RaceResult()

def Average(lst):
        return sum(lst) / len(lst)

class DataSampler:

    def __init__(self, numSamplesToAvg, DataSamplerFunc):
        self.array = [0]*numSamplesToAvg
        self.iterator = 0
        self.numSamplesToAvg = numSamplesToAvg
        self.DataSamplerFunc = DataSamplerFunc

    def GetDataSample(self):
        currentSample = self.DataSamplerFunc()
        self.array[self.iterator] = currentSample
        self.iterator += 1
        self.iterator = self.iterator % self.numSamplesToAvg
        return Average(self.array)

class Gauge:
    def __init__(self, image, min_value, max_value):
        self.dial = Image.open('images/needle.png')
        self.gauge = Image.open(image)
        self.min_degrees = 138
        self.max_degrees = 274
        self.min_value = min_value
        self.max_value = max_value
        
    def Clamp(self, n, minn, maxn):     
        return min(max(n, minn), maxn)
    
    def Update(self, value):        
        value = self.Clamp(value, self.min_value, self.max_value) # clamp value to range
        rotation = self.max_degrees * value / (self.max_value - self.min_value)
        self.dial_new = self.dial.rotate(self.min_degrees-rotation, resample=PIL.Image.BICUBIC ,center = (132,132))
        self.new_gauge = self.gauge.copy()
        self.new_gauge.paste(self.dial_new, mask=self.dial_new)  # Paste needle onto gauge
        self.gauge_im = ImageTk.PhotoImage(self.new_gauge)
        return self.gauge_im

class RaceInfo:
    def __init__(self, stage, topMessage, raceResult, raceState, bkgColor = "#000000", textColor="#FFFFFF", dataColor="#9DAAF2"):
        self.content = stage
        self.topMessage = Label(stage, text=topMessage, bg=bkgColor, fg=textColor, font=header2)
        self.topMessage.pack(pady=20)

        # Display user name  
        self.userNameLabel = Label(stage, text=raceResult.userName, bg=bkgColor, fg=dataColor, font=header2, relief=GROOVE, width=25)
        self.userNameLabel.pack(pady=15)

        # Insert timer
        self.timeFrame = Frame(stage, width=150, height=20, bg=bkgColor)

        # Insert Well Done Message
        self.wellDone = PhotoImage(file="images/wellDone2.png")
        self.congrats = Label(stage, image=self.wellDone, bg=bkgColor)

        # Insert Gauges
        self.power_gauge = Gauge('images/power_gauge.png', 0, 210)
        self.energy_gauge = Gauge('images/energy_gauge.png', 0, 17500)
        self.power_gauge_im = self.power_gauge.Update(raceResult.instantPower)
        self.energy_gauge_im = self.energy_gauge.Update(raceResult.totalEnergy)
        self.display_power_gauge = Label(self.content, image=self.power_gauge_im, bg=bkgColor)
        self.display_energy_gauge = Label(self.content, image=self.energy_gauge_im, bg=bkgColor)
        self.ShowGauges()

        # Race Timer
        self.countDownTitle = Label(self.timeFrame, text="Time Left: ", bg=bkgColor, fg=textColor, font=header1)
        self.countDownTitle.grid(row=0, column=0, pady=40)
        self.countDownLabel = Label(self.timeFrame, text="0 s", bg=bkgColor, fg=dataColor, font=header1)
        self.countDownLabel.grid(row=0, column=1, pady=40)

        # create grid with results
        columnWidth = 0
        self.resultsFrame = Frame(stage, width=150, height=20, bg=bkgColor)
        self.resultsFrame.place(anchor='s', relx=0.5, rely=0.75)

        # Race Info
        if raceState == 'race':
            headers = ["Inst Power (W)", 
                       "Max Power (W) ", 
                       "  Energy (J)  "]
        else:
            headers = ["Avg Power (W)",
                       "Max Power (W)",
                       "  Energy (J) "]
        for j in range(len(headers)):
            self.resultsHeader = Label(self.resultsFrame, text=headers[j],  width=columnWidth, font=header2, bg=bkgColor, fg=textColor)
            self.resultsHeader.grid(row=0, column=j, ipadx=50)
    
        textFont = header3
        # Display instant/average power
        self.avgPowerLabel = Label(self.resultsFrame, text="0", bg=bkgColor, fg=dataColor, font=textFont)
        self.avgPowerLabel.grid(row=1, column=0)

        # Display max power
        self.maxPowerLabel = Label(self.resultsFrame, text="0", bg=bkgColor, fg=dataColor, font=textFont)
        self.maxPowerLabel.grid(row=1, column=1)

        # Display Energy
        self.energyLabel = Label(self.resultsFrame, text="0", bg=bkgColor, fg=dataColor, font=textFont)
        self.energyLabel.grid(row=1, column=2)

        if raceState == 'race':
            self.ShowTimeFrame()
            self.HideCongrats()
        else:
            self.HideTimeFrame()
            self.ShowCongrats()

    def ShowGauges(self):
        self.display_power_gauge.place(anchor='n', relx=0.24, rely=0.25)
        self.display_energy_gauge.place(anchor='n', relx=0.76, rely=0.25)

    def UpdateGauges(self, raceResult, bkgColor):
        self.power_gauge_im = self.power_gauge.Update(raceResult.instantPower)
        self.energy_gauge_im = self.energy_gauge.Update(raceResult.totalEnergy)
        self.display_power_gauge.configure(image=self.power_gauge_im)
        self.display_energy_gauge.configure(image=self.energy_gauge_im)
        #self.ShowGauges()

    def ShowTimeFrame(self):
        self.timeFrame.place(anchor='n', relx=0.5, rely=0.3)

    def HideTimeFrame(self):
        self.timeFrame.place_forget()

    def ShowCongrats(self):
        self.congrats.place(anchor='n', relx=0.5, rely=0.31)

    def HideCongrats(self):
        self.congrats.pack_forget()

def SortData(inputArray, col):
    sortedArrayDesc = sorted(inputArray, key=lambda tup: tup[col], reverse=True)
    return sortedArrayDesc

def GetPowerData(recordsData):
    temp = copy.deepcopy(recordsData)
    [j.pop(1) for j in temp]
    return SortData(temp, 1)

def GetEnergyData(recordsData):
    temp = copy.deepcopy(recordsData)
    [j.pop(2) for j in temp]
    SortData(temp, 1)
    return SortData(temp, 1)

# load records from file
def LoadFromFile(fileName):
    inputFile = open(fileName, 'r')
    recordsData = []
    recordsList = inputFile.read()
    if os.stat(fileName).st_size == 0:
        print("File is empty")
        return [["Empty", 0, 0]]

    records = recordsList.split("\n")
    records = records[:-1] # discard last element which is garbage
    for record in records:
        data = []
        for i in record.split(","):
            if i.isdecimal():
                data.append(int(i))
            else:
                data.append(i)
        recordsData.append(data)
    inputFile.close()
    return SortData(recordsData, 1)

def DisplayRecords(stage, recordsData, labels, numRecords=10, bkgColor="#1A2238", textColor="#FFFFFF", dataColor="#9DAAF2"):
    # find total number of rows and columns in list
    numRows = len(recordsData)
    numCols = len(recordsData[0])

    columnWidth = 18
    # table
    frame = Frame(stage, width=150, height=20, bg=bkgColor)
    frame.pack(side=TOP, pady=15)
    
    for j in range(len(labels)):
        columnWidth = int(180*SCALE)
        anchorPlace = "e" if j==0 else "c"
        headerLabels = Label(frame, text=labels[j], width=2, anchor=anchorPlace, font=header3, bg=bkgColor, fg=textColor)
        headerLabels.grid(row=0, column=j, ipadx=columnWidth, pady=10)
    
    # code for creating table
    for i in range(numRows):
        positionLabel = Label(frame, font=body, anchor="e", bg=bkgColor, fg=dataColor)
        positionLabel.grid(row=i + 1, column=0, ipadx=columnWidth)
        positionLabel.configure(text=i+1)
        for j in range(0, numCols):
            E = Label(frame, anchor="c", font=body, bg=bkgColor, fg=dataColor)
            E.grid(row=i + 1, column=j+1, ipadx=columnWidth)
            E.configure(text=(recordsData[i][j]))
        if i >= numRecords-1:
            break
    return frame

def ConfigureStage(title, size, bkgColor="#1A2238"):
    stage = Tk()
    stage.title(title)
    stage.geometry(size)
    stage.resizable(True, True)
    stage.configure(bg=bkgColor)
    stage.attributes("-fullscreen", True)

    def CloseApp(e):
        sys.exit()

    def EnterFullScreen(e):
        stage.attributes("-fullscreen", True)

    def ExitFullScreen(e):
        stage.attributes("-fullscreen", False)

    # Bind keys to functions
    stage.bind('<Control-c>', lambda e: CloseApp(e))
    stage.bind('<F10>', lambda e: ExitFullScreen(e))
    stage.bind('<F11>', lambda e: EnterFullScreen(e))

    return stage

def InsertDate(stage, bkgColor="#1A2238", textColor="#FFFFFF"):
    dateTime = strftime('%d-%m-%Y')
    date = Label(stage, text=dateTime, font=body, bg=bkgColor, fg=textColor)
    date.place(anchor='ne', relx=0.95, rely=0.02)
    return date

def InsertTitle(stage, titleText, bkgColor="#1A2238", textColor="#FFFFFF"):
    title = Label(stage, text=titleText, font=header1)
    title.configure(bg=bkgColor, fg=textColor)
    title.pack(side=TOP, pady=10)
    return title

def InsertLogo(stage):
    logo = PhotoImage(file="images/LogotiposOficinas.png")
    pic = Label(stage, image = logo)
    pic.place(anchor='s', relx=0.5, rely=1.0)
    return (logo, pic)

def CreateInputBox(stage, instructionsText, bkgColor="#1A2238", textColor="#FFFFFF"):
    instructions = Label(stage, text=instructionsText, font=header2)
    instructions.configure(bg=bkgColor, fg=textColor)
    instructions.pack(side=TOP, pady=60)

    # create grid
    frame = Frame(stage, width=150, height=20, bg=bkgColor)
    frame.pack(pady=20)
    frame.configure(height=frame["height"], width=frame["width"])

    # Entry Widget
    label = Label(frame, text="Name: ", font=header2, bg=bkgColor, fg=textColor, width=5)
    label.grid(row=0, column=0)
    inputName = Entry(frame, font=header2, textvariable="Name", bg=bkgColor,
                      fg=textColor, insertbackground=textColor, borderwidth=5, relief=SUNKEN, insertborderwidth=10, width=25)
    inputName.grid(row=0, column=1)
    return (label,inputName)
