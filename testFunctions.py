import random
from helperFunctions import *

def testLoadRecords():
    return LoadRecords("rec.txt")

def testDisplayRecords():
    bkgColor = "#1A2238"
    stage = ConfigureStage("Energy Race", "1024x800", bkgColor)
    mainlst = []
    mainlst.append(["John", "29"])
    mainlst.append(["Kate", "15"])            
    mainlst.append(["Lucy", "34"])
    mainlst.append(["Mike", "62"])
    labels = ["#", "Name", "Age"]
    numRecords = 3
    DisplayRecords(stage, mainlst, labels, numRecords, bkgColor)
    stage.mainloop()

def testDataSampler():
    global counter
    global samples
    counter = 0
    samples = [5, 5, 5, 5, 5, 0, 0, 0, 0, 0]
    def PowerSamplerFunc():
        global counter 
        #sample = random.randint(0, 100)
        sample = samples[counter]
        counter += 1
        print("sample="+str(sample))
        return sample

    myDataSampler = DataSampler(5, PowerSamplerFunc)
    
    expected = [1, 2, 3, 4, 5, 4, 3, 2, 1, 0]
    for i in range(10):
        dataSample = myDataSampler.GetDataSample()
        print("Averaged Sample="+str(dataSample))
        assert dataSample == expected[i], "Failed testDataSampler"



def main():
    print("Test Suite")
    #testDisplayRecords()
    #records = testLoadRecords()
    #GetPowerData(records)
    testDataSampler()
    
if __name__ == "__main__":
    main()